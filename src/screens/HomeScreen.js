import React from 'react';
import {Text, StyleSheet, View, Image, TouchableOpacity} from 'react-native';

const HomeScreen = ({navigation}) => {
  const categories = [
    {
      id: 1,
      name: 'AC',
      img: require('../assets/ac.png'),
      url: 'AC',
    },
    {
      id: 2,
      name: 'DC',
      img: require('../assets/dc.png'),
      url: 'DC',
    },
    {
      id: 3,
      name: 'DC2',
      img: require('../assets/dc.png'),
      url: 'DC',
    },
  ];
  return (
    <View style={styles.root}>
      <View style={styles.imgWrapper}>
        <Image source={require('../assets/gmedia.png')} style={styles.logo} />
      </View>
      <View
        style={{
          width: '100%',
          alignItems: 'center',
          flexDirection: 'row',
          justifyContent: 'center',
          marginTop: 16,
        }}>
        {categories.map((category, index) => (
          <View key={index}>
            <TouchableOpacity onPress={() => navigation.navigate(category.url)}>
              <View style={styles.category}>
                <Image source={category.img} style={styles.ImageStyle} />
              </View>
            </TouchableOpacity>
            <Text style={styles.titleCategory}>{category.name}</Text>
          </View>
        ))}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    padding: 16,
  },
  imgWrapper: {
    flex: 1,
    justifyContent: 'center',
    width: '100%',
    flexDirection: 'row',
    marginBottom: 40,
  },
  logo: {
    width: 120,
    height: 40,
  },
  title: {
    textAlign: 'center',
    fontWeight: '700',
    fontSize: 20,
    marginBottom: 30,
  },
  subtitle: {
    fontSize: 14,
    fontWeight: '600',
    color: '#333333',
  },
  category: {
    width: 70,
    height: 70,
    borderWidth: 1,
    borderRadius: 5,
    marginHorizontal: 10,
    padding: 5,
  },
  titleCategory: {
    textAlign: 'center',
    fontWeight: '700',
    fontSize: 16,
    color: '#666666',
  },
  ImageStyle: {
    height: '100%',
    width: '100%',
    borderRadius: 5,
  },
});

export default HomeScreen;
