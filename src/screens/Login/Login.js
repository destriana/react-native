import React, {Component, useState} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Image,
} from 'react-native';

const Login = ({navigation}) => {
  const [show, setShow] = useState(false);

  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={styles.container}>
        <TextInput placeholder="username or Email" style={styles.formInput} />
        <View style={styles.passwordContainer}>
          <TextInput
            style={styles.inputStyle}
            autoCorrect={false}
            secureTextEntry={show}
            placeholder="Password"
            // value={this.state.password}
            // onChangeText={this.onPasswordEntry}
          />
          {show ? (
            <TouchableOpacity onPress={() => setShow(false)}>
              <Image
                source={require('../../assets/show.png')} //Change your icon image here
                style={styles.ImageStyle}
              />
            </TouchableOpacity>
          ) : (
            <TouchableOpacity onPress={() => setShow(true)}>
              <Image
                source={require('../../assets/hidden.png')} //Change your icon image here
                style={styles.ImageStyle}
              />
            </TouchableOpacity>
          )}
        </View>

        <TouchableOpacity
          style={styles.buttonSignIn}
          onPress={() => navigation.navigate('Welcome', {name: 'Destriana'})}>
          <Text style={{color: 'white', fontSize: 15}}>Sign In</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    paddingHorizontal: 14,
  },
  formInput: {
    height: 50,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 10,
    marginVertical: 10,
    borderColor: 'red',
    borderWidth: 1,
    width: '100%',
  },
  buttonSignIn: {
    height: 50,
    backgroundColor: '#673ab7',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    marginVertical: 10,
  },
  ImageStyle: {
    padding: 10,
    margin: 5,
    height: 25,
    width: 25,
    resizeMode: 'stretch',
    alignItems: 'center',
  },
  passwordContainer: {
    flexDirection: 'row',
    paddingBottom: 10,
    width: '100%',
  },
  inputStyle: {
    flex: 1,
    borderColor: 'red',
    borderWidth: 1,
    borderRadius: 20,
  },
});

export default Login;
