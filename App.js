import React from 'react';
import Login from './src/screens/Login/Login';
import Home from './src/screens/Home';
import AC from './src/screens/Ac';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

const Stack = createStackNavigator();
const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Sign In" component={Login} />
        <Stack.Screen name="Welcome" component={Home} />
        <Stack.Screen name="AC" component={AC} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
